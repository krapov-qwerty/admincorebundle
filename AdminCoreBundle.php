<?php
/**
 * This file is part of the MedTrainerCorePackage package.
 *
 * (c) MedTrainerFriends <https://bitbucket.org/medtrainerdevelopment/profile/members>
 * @copyright MedTrainer Company
 * @license CopyRight
 */
namespace MedTrainer\AdminCoreBundle;

use MedTrainer\AdminCoreBundle\DependencyInjection\Compiler\TwigPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AdminCoreBundle
 * {@inheritdoc}
 * @package MedTrainer\AdminCoreBundle
 */
class AdminCoreBundle extends Bundle
{
    /**
     * {@inheritdoc}
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new TwigPass());
    }
}