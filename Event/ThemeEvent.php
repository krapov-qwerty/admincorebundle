<?php
/**
 * This file is part of the MedTrainerCorePackage package.
 *
 * (c) MedTrainerFriends <https://bitbucket.org/medtrainerdevelopment/profile/members>
 * @copyright MedTrainer Company
 * @license CopyRight
 */
namespace MedTrainer\AdminCoreBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Base event class to make theme related events easier to detect
 */
class ThemeEvent extends Event
{
}
