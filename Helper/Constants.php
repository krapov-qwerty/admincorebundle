<?php
/**
 * This file is part of the MedTrainerCorePackage package.
 *
 * (c) MedTrainerFriends <https://bitbucket.org/medtrainerdevelopment/profile/members>
 * @copyright MedTrainer Company
 * @license CopyRight
 */
namespace MedTrainer\AdminCoreBundle\Helper;

interface Constants
{
    public const COLOR_AQUA = 'aqua';
    public const COLOR_GREEN = 'green';
    public const COLOR_RED = 'red';
    public const COLOR_YELLOW = 'yellow';
    public const COLOR_GREY = 'grey';
    public const COLOR_BLACK = 'black';

    /**
     * Used in:
     * - Model\NotificationModel
     * - Twig\AdminExtension
     */
    public const TYPE_SUCCESS = 'success';
    public const TYPE_WARNING = 'warning';
    public const TYPE_ERROR = 'error';
    public const TYPE_INFO = 'info';
}
