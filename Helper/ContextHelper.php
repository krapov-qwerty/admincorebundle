<?php
/**
 * This file is part of the MedTrainerCorePackage package.
 *
 * (c) MedTrainerFriends <https://bitbucket.org/medtrainerdevelopment/profile/members>
 * @copyright MedTrainer Company
 * @license CopyRight
 */
namespace MedTrainer\AdminCoreBundle\Helper;

class ContextHelper extends \ArrayObject
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->getArrayCopy();
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setOption($name, $value)
    {
        $this->offsetSet($name, $value);

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasOption($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed|null
     */
    public function getOption($name, $default = null)
    {
        return $this->offsetExists($name) ? $this->offsetGet($name) : $default;
    }
}
